
export class Review {
  StudentId: number
  CoursesAppliedFor: string
  Organisation: string
  Location: string
  Feedback: string
  ExperienceDate: number
  ReviewDate: number
  OrganisationUrl: string
  Rating: Number
  Title: string
}


export const dummyReviews = () => ([
  {
    Title: "Really good!",
    StudentId: 1,
    CoursesAppliedFor: "Journalism",
    Organisation: "UCAS",
    Location: "Cheltenham",
    Feedback: "I had a great time!",
    ExperienceDate: Date.now(),
    ReviewDate: Date.now(),
    OrganisationUrl: "www.Ucas.com",
    Rating: 8
  },
  { Title: "It was okay", StudentId: 1, CoursesAppliedFor: "Nursing", Organisation: "Gloucester Hospital", Rating: 3, Location: "Gloucestershire", Feedback: "I got to give people medicine", ExperienceDate: Date.now(), ReviewDate: Date.now(), OrganisationUrl: "www.glosHospital.org.uk" },
  { Title: "Best week of my life!", StudentId: 2, CoursesAppliedFor: "Medicine", Organisation: "Gloucester Hospital", Rating: 9, Location: "Gloucestershire", Feedback: "X-rays gave me super powers", ExperienceDate: Date.now(), ReviewDate: Date.now(), OrganisationUrl: "www.glosHospital.org.uk" },
  { Title: "Good experience", StudentId: 3, CoursesAppliedFor: "Art", Organisation: "The Wilson", Rating: 10, Location: "Cheltenham", Feedback: "Managed to not paint outside the lines", ExperienceDate: Date.now(), ReviewDate: Date.now(), OrganisationUrl: "https://www.cheltenhammuseum.org.uk/" },
  { Title: "Boring", StudentId: 4, CoursesAppliedFor: "Sports Coaching", Organisation: "Gloucester Rugby", Rating: 2, Location: "GLoucester", Feedback: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaalajdasuhjiodhasiodfhaohfosdhiodahjodasjnodha aiodasodhasiohdoiashdoas dasd", ExperienceDate: Date.now(), ReviewDate: Date.now(), OrganisationUrl: "https://www.GRFC.co.uk/" },
  { Title: "Awful", StudentId: 5, CoursesAppliedFor: "Music", Organisation: "Royal Albert Hall", Rating: 1, Location: "London", Feedback: "They only played really old music", ExperienceDate: Date.now(), ReviewDate: Date.now(), OrganisationUrl: "https://www.royalalberthall.com/" },
  { Title: "Amazing!", StudentId: 6, CoursesAppliedFor: "Music", Organisation: "Royal Albert Hall", Rating: 10, Location: "London", Feedback: "They only played really old music", ExperienceDate: Date.now(), ReviewDate: Date.now(), OrganisationUrl: "https://www.royalalberthall.com/" },
  { Title: "Flawless", StudentId: 7, CoursesAppliedFor: "English", Organisation: "Royal Albert Hall", Rating: 10, Location: "London", Feedback: "They only played really old music", ExperienceDate: Date.now(), ReviewDate: Date.now(), OrganisationUrl: "https://www.royalalberthall.com/" },
  { Title: "Decent enough", StudentId: 8, CoursesAppliedFor: "Music", Organisation: "Royal Albert Hall", Rating: 6, Location: "London", Feedback: "They only played really old music", ExperienceDate: Date.now(), ReviewDate: Date.now(), OrganisationUrl: "https://www.royalalberthall.com/" },
  { Title: "Meh", StudentId: 9, CoursesAppliedFor: "Music", Organisation: "Royal Albert Hall", Rating: 4, Location: "London", Feedback: "They only played really old music", ExperienceDate: Date.now(), ReviewDate: Date.now(), OrganisationUrl: "https://www.royalalberthall.com/" },
  { Title: "Pretty good", StudentId: 9, CoursesAppliedFor: "Teaching", Organisation: "GCHQ", Rating: 7, Location: "Gloucester", Feedback: "Good experience", ExperienceDate: Date.now(), ReviewDate: Date.now(), OrganisationUrl: "Gloucester College" },
  { Title: "Flawless", StudentId: 7, CoursesAppliedFor: "English", Organisation: "GCHQ", Rating: 10, Location: "London", Feedback: "They only played really old music", ExperienceDate: Date.now(), ReviewDate: Date.now(), OrganisationUrl: "https://www.royalalberthall.com/" },
  { Title: "Decent enough", StudentId: 8, CoursesAppliedFor: "Music", Organisation: "Gloucester College", Rating: 6, Location: "London", Feedback: "They only played really old music", ExperienceDate: Date.now(), ReviewDate: Date.now(), OrganisationUrl: "https://www.royalalberthall.com/" },
  { Title: "Meh", StudentId: 9, CoursesAppliedFor: "Music", Organisation: "GCHQ", Rating: 4, Location: "London", Feedback: "They only played really old music", ExperienceDate: Date.now(), ReviewDate: Date.now(), OrganisationUrl: "https://www.royalalberthall.com/" },
  { Title: "Pretty good", StudentId: 9, CoursesAppliedFor: "Teaching", Organisation: "GCHQ", Rating: 7, Location: "Gloucester", Feedback: "Good experience", ExperienceDate: Date.now(), ReviewDate: Date.now(), OrganisationUrl: "Gloucester College" }

])


