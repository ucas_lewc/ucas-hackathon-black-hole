import { dummyReviews, Review } from "./review"

export class Organisation {
  Name: string
  Location: string
  Reviews: Review[]
  AverageRating: number
  BackgroundImage : string
}

export const dummyOrganisations: Organisation[] = ([
  {
    Name: "UCAS",
    Location: "Cheltenham",
    Reviews: dummyReviews().filter(x => x.Organisation == "UCAS"),
    AverageRating: dummyReviews().filter(x => x.Organisation == "UCAS").reduce((a, b) => a + b.Rating, 0) / dummyReviews().filter(x => x.Organisation == "UCAS").length
    ,BackgroundImage: "https://media.glassdoor.com/l/81/59/2e/16/rosehill-front.jpg"
  },
  {
    Name: "Gloucester College",
    Location: "Gloucestershire",
    Reviews: dummyReviews().filter(x => x.Organisation == "Gloucester College"),
    AverageRating: dummyReviews().filter(x => x.Organisation == "Gloucester College").reduce((a, b) => a + b.Rating, 0) / dummyReviews().filter(x => x.Organisation == "Gloucester College").length
    ,BackgroundImage: "https://upload.wikimedia.org/wikipedia/commons/8/8e/Gloucestershire_College%2C_Llanthony_Road.jpg"
  },
  {
    Name: "Gloucester Hospital",
    Location: "Gloucestershire",
    Reviews: dummyReviews().filter(x => x.Organisation == "Gloucester Hospital"),
    AverageRating: dummyReviews().filter(x => x.Organisation == "Gloucester Hospital").reduce((a, b) => a + b.Rating, 0) / dummyReviews().filter(x => x.Organisation == "Gloucester Hospital").length
    ,BackgroundImage:"https://www.gazetteseries.co.uk/resources/images/10809987.jpg?display=1&htype=0&type=responsive-gallery"
  },
  {
    Name: "Royal Albert Hall",
    Location: "London",
    Reviews: dummyReviews().filter(x => x.Organisation == "Royal Albert Hall"),
    AverageRating: dummyReviews().filter(x => x.Organisation == "Royal Albert Hall").reduce((a, b) => a + b.Rating, 0) / dummyReviews().filter(x => x.Organisation == "Royal Albert Hall").length,
    BackgroundImage: "https://upload.wikimedia.org/wikipedia/commons/9/93/Royal_Albert_Hall%2C_London_-_Nov_2012.jpg"
  },
  {
    Name: "GCHQ",
    Location: "unknown",
    Reviews: dummyReviews(),
    AverageRating: dummyReviews().filter(x => x.Organisation == "GCHQ").reduce((a, b) => a + b.Rating, 0) / dummyReviews().filter(x => x.Organisation == "GCHQ").length
  ,BackgroundImage: "https://i2-prod.gloucestershirelive.co.uk/incoming/article2760759.ece/ALTERNATES/s810/2_GCHQ.jpg"
  }])


export class org {
  took: number
  timed_out: boolean
  hits: hits
}

export class hits {
  hits: hits2
}

export class hits2 {
  organisations: source
}

export class source {
  review: review
}

export class review {
  id: string
  location: string
  rating: number
  summary: string
  content: string
  placementDate: string
  reviewDate: string
  course: string
}
