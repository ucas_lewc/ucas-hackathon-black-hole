import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { OrganisationPageComponent } from './organisation-page/organisation-page.component';
import { ReviewFormComponent } from './review-form/review-form.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReviewPageComponent } from './review-page/review-page.component';
import { WebService } from 'src/services/web.service';

@NgModule({
  declarations: [
    AppComponent,
    SearchPageComponent,
    HeaderComponent,
    OrganisationPageComponent,
    ReviewFormComponent,
    ReviewPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [WebService],
  bootstrap: [AppComponent]
})
export class AppModule { }
