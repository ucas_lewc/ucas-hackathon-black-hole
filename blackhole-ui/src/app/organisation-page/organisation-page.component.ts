import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { dummyOrganisations, Organisation } from 'src/common/organisation';

@Component({
  selector: 'app-organisation-page',
  templateUrl: './organisation-page.component.html',
  styleUrls: ['./organisation-page.component.scss']
})
export class OrganisationPageComponent implements OnInit {
  organisationName
  private sub: any
  constructor(private route: ActivatedRoute) { }
  organisation: Organisation
  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.organisationName = params['organisationName']
      this.organisation = dummyOrganisations.filter(x => x.Name == this.organisationName)[0]

    })

  }

}
