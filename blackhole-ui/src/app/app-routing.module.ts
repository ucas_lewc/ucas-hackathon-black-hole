import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrganisationPageComponent } from './organisation-page/organisation-page.component';
import { ReviewFormComponent } from './review-form/review-form.component';
import { SearchPageComponent } from './search-page/search-page.component';


const routes: Routes = [
  { path: 'review', component: ReviewFormComponent },
  { path: 'organisation/:organisationName', component: OrganisationPageComponent },
  { path: '**', component: SearchPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
