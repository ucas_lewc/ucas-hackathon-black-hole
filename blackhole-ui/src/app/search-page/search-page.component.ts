import { Component, OnInit } from '@angular/core';
import { dummyOrganisations } from 'src/common/organisation';
import { WebService } from 'src/services/web.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit {
  courseSearch
  locationSearch
  return
  organisations = dummyOrganisations
  testing
  constructor(private webService: WebService) {
    this.webService.getCourses().subscribe(data => { this.return = data.hits.hits.organisations })
  }

  ngOnInit() {

  }

  onSubmit() {
    console.log(this.organisations);
    console.log(this.locationSearch)
    if (this.locationSearch) {
      this.organisations = dummyOrganisations.filter(x => x.Location == this.locationSearch)
    } else {
      this.organisations = dummyOrganisations
    }

  }
}
