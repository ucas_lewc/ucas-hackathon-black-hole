import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { org } from "src/common/organisation";

@Injectable()
export class WebService {
  headers = {
    Authorization: 'Basic YmxhY2tob2xlYWRtaW46NGEjNHVSajA1OWRo'
  }

  body = {
    "query": {
        "match_all": {}
    }
  }

  requestOptions = {
    headers: new HttpHeaders(this.headers),
    body : this.body

  }

  constructor(private http: HttpClient) {
  }

  getCourses() {
    return this.http.get<org>("https://search-black-hole-2-iwklvjwtfobjv6353fcolrmmha.eu-west-1.es.amazonaws.com/orgs/_search", this.requestOptions )
  }
}
