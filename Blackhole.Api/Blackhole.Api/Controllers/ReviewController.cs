﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Blackhole.DataAccess.Models;
using Nest;
using Newtonsoft.Json;
using SearchRequest = Blackhole.DataAccess.Models.SearchRequest;

namespace Blackhole.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReviewController : ControllerBase
    {
        private readonly IElasticClient _client;
        // private const string elasticUri = "https://vpc-black-hole-bhjarantnwne3wtag5vqab4jf4.eu-west-1.es.amazonaws.com";
        private const string ElasticUri = "https://localhost:9200";

        public ReviewController()
        {
            _client = new ElasticClient(
                new Uri(ElasticUri));
        }

        [HttpPost]
        public IActionResult Post(SearchRequest request)
        {
            var searchResponse = _client.Search<Review>(s => s
                .Index("orgs").MatchAll());


            if (searchResponse.IsValid)
            {
                return Ok(searchResponse.Documents);
            }
            else
            {
                throw searchResponse.OriginalException;
            }
        }

        [HttpGet]
        public string Get()
        {
            List<Review> reviews = new List<Review>();

            var review = new Review
            {
                Id = 1,
                CoursesAppliedFor = "Journalism",
                Organisation = "UCAS",
                Location = "Cheltenham",
                ExperienceDate = DateTime.Parse("Jan 1, 2018").Date,
                ReviewDate = DateTime.Now.Date,
                ReviewTitle = "What an experience",
                Feedback = "I had a great time!",
                OrganisationUrl = "UCAS.com",
                OrganisationLogoPath =
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/UCAS_logo.svg/1200px-UCAS_logo.svg.png"
            };

            var review2 = new Review
            {
                Id = 2,
                CoursesAppliedFor = "English",
                Organisation = "KFC",
                Location = "Swansea",
                ExperienceDate = DateTime.Parse("Sep 15, 2015").Date,
                ReviewDate = DateTime.UtcNow.Date,
                ReviewTitle = "Chicken-tastic",
                Feedback = "An experience I'll never forget",
                OrganisationUrl = "KFC.com"
            };

            reviews.Add(review);
            reviews.Add(review2);

            string json = JsonConvert.SerializeObject(reviews);

            return json;
        }
    }
}
