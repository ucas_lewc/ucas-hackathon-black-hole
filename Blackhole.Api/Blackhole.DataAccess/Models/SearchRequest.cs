﻿namespace Blackhole.DataAccess.Models
{
    public class SearchRequest
    {
        public string CoursesAppliedFor { get; set; }
        public string Organisation { get; set; }
        public string Location { get; set; }
    }
}
