﻿using System;

namespace Blackhole.DataAccess.Models
{
    public class Review
    {
        public int Id { get; set; }
        public string CoursesAppliedFor { get; set; }
        public string Organisation { get; set; }
        public string Location { get; set; }
        public string ReviewTitle { get; set; }
        public string Feedback { get; set; }
        public DateTime ExperienceDate { get; set; }
        public DateTime ReviewDate { get; set; }
        public string OrganisationUrl { get; set; }
        public string OrganisationLogoPath { get; set; }
        public string BackgroundImagePath { get; set; }
    }
}
