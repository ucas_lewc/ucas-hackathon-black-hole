import axios from 'axios';
import { green, red } from 'chalk';
import faker from 'faker';
import jsf from 'json-schema-faker';
import orgsSchema from './schema/orgs';
import reviewsSchema from './schema/reviews';

const fs = require('fs');

jsf.extend('faker', () => faker);

const compiledOrgsSchema = jsf.generate(orgsSchema);

const orgs: Array<any> = compiledOrgsSchema.orgs;

// const orgIds = orgs.map((o) => o.id);

const esUrl =
  'https://search-black-hole-2-iwklvjwtfobjv6353fcolrmmha.eu-west-1.es.amazonaws.com/';
const esc = axios.create({
  baseURL: esUrl,
  proxy: false,
  headers: {
    Authorization: 'Basic YmxhY2tob2xlYWRtaW46NGEjNHVSajA1OWRo',
  },
});
const orgsIndexName = 'orgs';

const populateIndices = async () => {
  // delete the existing index
  await esc.delete(orgsIndexName).catch((reason) => console.log(red(reason)));

  // create a mapping for the nested reviews
  await esc
    .put(`${orgsIndexName}`, {
      mappings: {
        properties: {
          reviews: {
            type: 'nested',
          },
        },
      },
    })
    .catch((reason) => console.log(red(reason)));

  // index all the orgs
  await axios.all(
    orgs.map((o) => {
      // create a set of reviews for the org
      const compiledReviewsSchema = jsf.generate(reviewsSchema);
      o.reviews = compiledReviewsSchema.reviews;

      // post the data to the index
      return esc
        .post(`${orgsIndexName}/_doc/${o.id}?pretty`, o, {
          headers: { 'Content-Type': 'application/json' },
        })
        .catch((reason) => console.log(red(reason)));
    }),
  );
};

const writeToFile = () => {
  const json = JSON.stringify({
    orgs,
  });

  fs.writeFile('./db.json', json, (err) => {
    if (err) {
      console.log(red(err.message));
    } else {
      console.log(green('Mock API data generated.'));
    }
  });
};

populateIndices().then(writeToFile);
