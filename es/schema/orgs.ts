const orgsSchema = {
  type: 'object',
  properties: {
    orgs: {
      type: 'array',
      minItems: 10,
      maxItems: 20,
      uniqueItems: true,
      items: {
        type: 'object',
        properties: {
          id: {
            type: 'string',
            faker: 'random.uuid',
          },
          name: {
            type: 'string',
            faker: 'company.companyName',
          },
          location: {
            type: 'string',
            faker: 'address.city',
          },
          tagline: {
            type: 'string',
            faker: 'company.bs',
          },
          description: {
            type: 'string',
            faker: 'lorem.paragraph',
          },
          url: {
            type: 'string',
            faker: 'internet.url',
          },
          logoUrl: {
            type: 'string',
            faker: { 'image.animals': [100, 100] },
          },
          backgroundUrl: {
            type: 'string',
            faker: 'image.business',
          },
        },
        required: [
          'id',
          'name',
          'tagline',
          'description',
          'url',
          'logoUrl',
          'backgroundUrl',
        ],
      },
    },
  },
  required: ['orgs'],
};

export default orgsSchema;
