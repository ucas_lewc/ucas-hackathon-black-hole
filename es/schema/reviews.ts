const feedback = [
  "I am a mature student.  In my work experience, I was helping the consultants to monitor and care for patients in hospitals and clinics.  I also worked in the pharmacy department to understand and prescribe and review patient's medication.",
  'I was a project coordinator responsible for ensuring on-time deliverables of medication for all cancer patients. It is also my responsbility to keep track and meet deadlines weekly.  I set up and led team meetings with the project team to streamline the process of medication delivery.',
  "I would like to become a doctor who specialised in Neurology.  The two weeks of work experience provided me with a good understanding of the behaviour of dementia patients. Alzheimer's society is a good company to get work experience.",
  'I enjoyed working with children and had a good opportunity to gain 10 days of work experience with Elstow school.  I worked with a SEN teacher to provide support to student with special needs.   It can be challenging at times but it was a good experience.',
  'I have few weeks of work experience with the Regency Manor care home.  This is a care home for both dementia and non-dementia residents.  I Worked with the non-demential group and I hope to gain some trainings working with the demential residents one day.',
  'I have a two weeks of work experience.  During my two weeks here, I noticed doctors worked long hours.  Their shifts are planned on a rota system, including overnight and weekend work.  I am not sure if this is what I want.',
  'It was fun working with the youth employment agency based in Doncaster. I helped the young people to write and present reports, managed budgets, applied for project funding etc.',
];

const reviewsSchema = {
  type: 'object',
  properties: {
    reviews: {
      type: 'array',
      minItems: 5,
      maxItems: 10,
      uniqueItems: true,
      items: {
        type: 'object',
        properties: {
          id: {
            type: 'string',
            faker: 'random.uuid',
          },
          rating: {
            type: 'number',
            faker: {
              'random.number': 5,
            },
          },
          summary: {
            type: 'string',
            faker: 'hacker.phrase',
          },
          content: {
            enum: feedback,
          },
          course: {
            enum: ['Medicine', 'Teaching', 'Social work', 'Nursing'],
          },
          placementDate: {
            type: 'string',
            faker: {
              'date.past': 1,
            },
          },
          reviewDate: {
            type: 'string',
            faker: {
              'date.past': 10,
            },
          },
        },
        required: [
          'id',
          'location',
          'rating',
          'summary',
          'content',
          'placementDate',
          'reviewDate',
        ],
      },
    },
  },
  required: ['reviews'],
};

export default reviewsSchema;
